const express = require("express");
const { MongoClient } = require("mongodb");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const multer = require("multer");
const fs = require('fs');
const shortid = require('shortid');
const app = express();
const uri = "mongodb://localhost:27017";
const client = new MongoClient(uri);
const upload = multer({ dest: "uploaded-photos/" })
let db = null;
const saltRounds = 10;
let jwtSecret = "deb";

app.use(express.json());
app.use("/uploaded-photos", express.static("uploaded-photos/"));

function getJWT(payload) {
    const options = {
        expiresIn: "3600s"
    }
    return jwt.sign(payload, jwtSecret, options);
}

function createService(owner, name, description) {
    return {
        serviceId: shortid.generate(),
        serviceOwner: owner,
        name: name,
        description: description,
        availability: true,
        bookedBy: [],
        pictures: [],
        likeCount: 1,
        comments: [],
        isDelete: false,
        bookingHistory: []
    }
}

function auth(req, res, next) {
    if (!req.headers.authorization) {
        return res.json({
            success: false,
            message: "please login"
        })
    }
    const authorizationHeader = req.headers.authorization;
    const splittedAuth = authorizationHeader.split(" ");

    if (splittedAuth[0] === "Bearer") {
        let token = splittedAuth[1];
        jwt.verify(token, jwtSecret, function (err, decoded) {
            if (err) {
                res.status(401);
                return res.json({
                    success: false,
                    message: "please login again"
                })
            }
            req.tokenPayload = decoded;
            next();
        });
    }
}

async function getServiceFromDB(paramService) {
    let paramServiceId = paramService;
    if (!paramServiceId) {
        // null
        return null;
    }

    const query = { "serviceId": paramService };
    const matchedService = await db.collection("services").findOne(query);
    return matchedService;
}

function checkIfOwner(tokenOwner, serviceOwner) {
    return tokenOwner === serviceOwner;
}

async function checkMemberExist(username) {
    const query = { "name": username };
    const member = await db.collection("member").findOne(query);
    return member
}

function formatDate(newDate) {

    function addZero(num) {
        let str = num.toString();
        if (str.length === 1) {
            return "0" + str;
        }
        return str;
    }
    let year = newDate.getFullYear();
    let month = addZero(newDate.getMonth() + 1);
    let date = addZero(newDate.getDate());
    let hour = addZero(newDate.getHours());
    let minute = addZero(newDate.getMinutes());
    let second = addZero(newDate.getSeconds());

    return `${year}-${month}-${date} ${hour}:${minute}:${second}`;
}

async function initizeDB() {
    try {
        await client.connect();
        db = await client.db("mongo_assignment3_db");
    } catch (error) {
        console.log(error);
    }
}


// app.get("/readTest", async (req, res) => {
//     const cursor = db.collection("test").find();
//     const result = await cursor.toArray();
//     console.log(result);
//     res.json(result);
// })

//register
app.post("/register", async (req, res) => {
    const username = req.body.username;
    const password = req.body.password;


    //if exist member, ask user to input another name
    let checkExistMember = await checkMemberExist(username);
    console.log(checkExistMember);
    if (checkExistMember) {
        return res.json({
            success: false,
            message: "This name has been registered. please use another name",
        })

    };
    //hash password
    bcrypt.hash(password, saltRounds, function (err, hash) {
        if (err) {
            return res.json({
                success: false,
                message: "please register again"
            })
        }

        const newMember = {
            name: username,
            password: hash,
        };


        (async () => {
            const newMemberInDB = await db.collection("member").insertOne(newMember);
            //generate Token
            let token = getJWT({
                username: username
            })

            return res.json({
                token,
                newMemberInDB

            })
        })();
    });
})

//login
app.post("/login", async (req, res) => {
    const username = req.body.username;
    const password = req.body.password;

    let matchedMember = await checkMemberExist(username);
    if (matchedMember === null) {
        return res.json({
            success: false,
            message: "no such member"
        })
    }

    bcrypt.compare(password, matchedMember.password, function (err, result) {
        // result == true
        if (result) {
            let token = getJWT({
                username: username
            })

            return res.json({
                token
            })
        } else {
            res.json({
                success: false,
                message: "wrong password"
            })
        }
    });
})

//post new service
app.post("/service", auth, async (req, res) => {

    const owner = req.tokenPayload.username;
    const name = req.body.name;
    const description = req.body.description;
    const newService = createService(owner, name, description);
    const result = await db.collection("services").insertOne(newService);
    return res.json({
        success: true,
        result: result
    })
})

//post photos
app.post("/service/:serviceId/photos", auth, upload.array("photos", 10), async (req, res) => {
    // check if owner , body should include service id
    if (!req.files) {
        return res.json({
            success: false,
            message: "upload fail"
        });
    }

    let updateService = await getServiceFromDB(req.params.serviceId);

    if (!updateService) {
        return res.json({
            success: false,
            message: "no such service"
        });
    }

    if (!checkIfOwner(req.tokenPayload.username, updateService.serviceOwner)) {
        return res.json({
            success: false,
            message: "only owner can perform this action"
        });
    }
    updateService.pictures = req.files.map(file => file.path);

    const filter = { "serviceId": req.params.serviceId };
    const update = {
        $push: { "pictures": { $each: updateService.pictures } }
    }
    const result = await db.collection("services").updateOne(filter, update);
    return res.json({
        success: true,
        message: `uploaded, files path for ${updateService.name} is ${updateService.pictures}`
    });

})

//get all services
app.get("/service", async (req, res) => {

    const cursor = db.collection("services").find();
    const servicesResult = await cursor.toArray();
    return res.json({
        servicesResult
    })
})

//get one service 
app.get("/service/:serviceId", async (req, res) => {
    let service = await getServiceFromDB(req.params.serviceId);
    if (!service) {
        return res.json({
            success: false,
            message: "no such service"
        });
    }

    return res.json({
        service
    })
})


//update One Service
app.patch("/service/:serviceId", auth, async (req, res) => {
    let updateService = await getServiceFromDB(req.params.serviceId);
    console.log(updateService);
    if (!updateService) {
        return res.json({
            success: false,
            message: "no such service"
        });
    }

    if (!checkIfOwner(req.tokenPayload.username, updateService.serviceOwner)) {
        return res.json({
            success: false,
            message: "only owner can perform this action"
        });
    }

    let updateItems = Object.keys(req.body.updateDetail);
    let updateDB = {};
    for (let updateItem of updateItems) {
        if (!updateService.hasOwnProperty(updateItem)) {

            continue;
        }
        updateDB[updateItem] = req.body.updateDetail[updateItem];
    }
    const filter = { "serviceId": req.params.serviceId };
    const update = { $set: updateDB };
    const result = await db.collection("services").updateOne(filter, update);

    return res.json({
        success: true,
        message: result
    })
})

//toggle availability
app.patch("/service/:serviceId/toggleAvailability", auth, async (req, res) => {
    let updateService = await getServiceFromDB(req.params.serviceId);
    if (!updateService) {
        return res.json({
            success: false,
            message: "no such service"
        });
    }

    if (!checkIfOwner(req.tokenPayload.username, updateService.serviceOwner)) {
        return res.json({
            success: false,
            message: "only owner can perform this action"
        });
    }

    const filter = { "serviceId": req.params.serviceId };
    const update = { $set: { "availability": !updateService.availability } };
    const result = await db.collection("services").updateOne(filter, update);

    res.json({
        success: true,
        message: result
    })

})

//like
app.patch("/service/:serviceId/like", auth, async (req, res) => {
    let updateService = await getServiceFromDB(req.params.serviceId);
    if (!updateService) {
        return res.json("no such service");
    }
    const filter = { "serviceId": req.params.serviceId };
    const update = { $inc: { "likeCount": 1 } };
    const result = await db.collection("services").updateOne(filter, update);

    res.json({
        success: true,
        message: result
    })
})

//comment
app.post("/service/:serviceId/comment", auth, async (req, res) => {
    let updateService = await getServiceFromDB(req.params.serviceId);
    if (!updateService) {
        return res.json({
            success: false,
            message: "no such service"
        });
    }
    if (!req.body.content) {
        return res.json({
            success: false,
            message: "please input comments"
        });
    }
    let newComment = {
        author: req.tokenPayload.username,
        content: req.body.content,
        dateTime: formatDate(new Date())
    }

    const filter = { "serviceId": req.params.serviceId };
    const update = {
        $push: { "comments": newComment }
    }
    const result = await db.collection("services").updateOne(filter, update);
    return res.json({
        success: true,
        message: result
    });

})

//book
app.post("/service/:serviceId/book", auth, async (req, res) => {
    let updateService = await getServiceFromDB(req.params.serviceId);
    if (!updateService) {
        return res.json({
            success: false,
            message: "no such service"
        });
    }

    if (checkIfOwner(req.tokenPayload.username, updateService.serviceOwner)) {
        return res.json({
            success: false,
            message: "owner cannot do booking"
        });
    }

    if (updateService.bookedBy.length > 0) {
        return res.json({
            success: false,
            message: "already booked"
        });
    }

    let bookingObj = {
        username: req.tokenPayload.username,
        bookingTime: new Date()
    }

    const filter = { "serviceId": req.params.serviceId };
    const update = { $set: { "availability": false }, $push: { "bookedBy": bookingObj } };
    const result = await db.collection("services").updateOne(filter, update);

    return res.json({
        success: true,
        message: result
    });
})

//remove booking
app.patch("/service/:serviceId/remove-booking", auth, async (req, res) => {
    let updateService = await getServiceFromDB(req.params.serviceId);
    if (!updateService) {
        return res.json({
            success: false,
            message: "no such service"
        });
    }

    if (!checkIfOwner(req.tokenPayload.username, updateService.serviceOwner)) {
        return res.json({
            success: false,
            message: "only owner can perform this action"
        });
    }

    if (updateService.bookedBy.length === 0) {
        return res.json({
            success: false,
            message: "it is not booked"
        });
    }

    const filter = { "serviceId": req.params.serviceId };
    const update = {
        $set: { "availability": true, "bookedBy": [] },
        $push: { "bookingHistory": updateService.bookedBy[0] }
    };
    const result = await db.collection("services").updateOne(filter, update);

    return res.json({
        success: true,
        message: result
    });

})

//delete service
app.patch("/service/:serviceId/delete", auth, async (req, res) => {
    let updateService = await getServiceFromDB(req.params.serviceId);
    if (!updateService) {
        return res.json({
            success: false,
            message: "no such service"
        });
    }

    if (!checkIfOwner(req.tokenPayload.username, updateService.serviceOwner)) {
        return res.json({
            success: false,
            message: "only owner can perform this action"
        });
    }

    if (updateService.bookedBy.length > 0) {
        return res.json({
            success: false,
            message: "already booked, cannot delete now"
        });
    }

    if (updateService.pictures.length > 0) {
        console.log(updateService.pictures);
        // Function to get current filenames
        // in directory with specific extension
        function getFilesInDirectory() {
            console.log("\nFiles present in directory:");
            let files = fs.readdirSync(__dirname);
            files.forEach(file => {
                console.log(file);
            });
        }
        for (let filePath of updateService.pictures) {
            // Get the files in current directory
            // before deletion
            //getFilesInDirectory();

            // Delete example_file.txt
            fs.unlink(filePath, (err => {
                if (err) console.log(err);
                else {
                    console.log(`\nDeleted file: ${filePath}`);

                    // Get the files in current directory
                    // after deletion
                    // 
                    FilesInDirectory();
                }
            }));
        }
    }

    const filter = { "serviceId": req.params.serviceId };
    const update = { $set: { "isDelete": true, "pictures": [] } };
    const result = await db.collection("services").updateOne(filter, update);

    res.json({
        success: true,
        message: result
    })
})

initizeDB().then(() => {
    app.listen(3000, () => {
        console.log("Example app listening on port 3000!");
    });
}).catch(console.error);
